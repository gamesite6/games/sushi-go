mod public_state;
mod secret_state;

use public_state::PublicPlayerState;
pub use public_state::PublicState;
pub use secret_state::{PrivateState, SecretState};

use rand::prelude::*;
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};
use std::convert::TryFrom;

pub type PlayerId = i32;
pub type Score = i8;

#[derive(Clone, Copy, Debug, Deserialize, Serialize, Eq, PartialEq)]
pub struct CardId(u8);

impl CardId {
    pub fn card_type(&self) -> Card {
        Card::try_from(*self).expect(&format!("no card type for card id {}", self.0))
    }
    pub fn rice_balls(&self) -> u8 {
        match Card::try_from(*self) {
            Ok(Card::RiceBall(rolls)) => rolls,
            _ => 0,
        }
    }
    pub fn score_value(&self) -> Score {
        match Card::try_from(*self) {
            Ok(Card::Sushi(value)) => value,
            _ => 0,
        }
    }

    pub fn is_takeout_box(&self) -> bool {
        Card::try_from(*self) == Ok(Card::TakeoutBox)
    }

    pub fn is_dessert(&self) -> bool {
        Card::try_from(*self) == Ok(Card::Dessert)
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Card {
    Tempura,
    Dango,
    Gyoza,
    RiceBall(u8),
    Sushi(Score),
    Dessert,
    Tea,
    TakeoutBox,
}

impl Card {
    pub fn is_nigiri(&self) -> bool {
        match self {
            Card::Sushi(_) => true,
            _ => false,
        }
    }
}

pub fn score_desserts(players: &[PlayerState]) -> HashMap<PlayerId, Score> {
    let mut scores: HashMap<PlayerId, Score> = players.iter().map(|p| (p.id, 0)).collect();

    let dessert_counts: HashMap<PlayerId, usize> = players
        .iter()
        .map(|p| {
            (
                p.id,
                p.picked
                    .iter()
                    .filter(|cs| cs.len() == 1 && cs[0].card_type() == Card::Dessert)
                    .count(),
            )
        })
        .collect();

    {
        let max_dessert = dessert_counts.values().max().copied().unwrap_or(0);
        let max_dessert_players = dessert_counts
            .iter()
            .filter(|&(_player_id, n)| *n == max_dessert)
            .collect::<Vec<_>>();
        let point_share = Score::try_from(6 / max_dessert_players.len()).unwrap();

        for (&player_id, _) in max_dessert_players {
            let score = scores.entry(player_id).or_insert(0);
            *score += point_share;
        }
    }

    // noone loses points for desserts in a 2 player game
    if players.len() > 2 {
        let min_dessert = dessert_counts.values().min().copied().unwrap_or(0);
        let min_dessert_players = dessert_counts
            .iter()
            .filter(|&(_player_id, n)| *n == min_dessert)
            .collect::<Vec<_>>();
        let penalty_share = Score::try_from(6 / min_dessert_players.len()).unwrap();

        for (&player_id, _) in min_dessert_players {
            let score = scores.entry(player_id).or_insert(0);
            *score -= penalty_share;
        }
    }

    scores
}

pub fn score_round(players: &[PlayerState]) -> HashMap<PlayerId, Score> {
    let mut scores: HashMap<PlayerId, Score> = players.iter().map(|p| (p.id, 0)).collect();

    /* rice balls */
    {
        let rice_ball_counts: HashMap<PlayerId, u8> = players
            .iter()
            .map(|p| {
                (
                    p.id,
                    p.picked
                        .iter()
                        .map(|c| if c.len() == 1 { c[0].rice_balls() } else { 0 })
                        .sum(),
                )
            })
            .collect();

        let max_rice_ball_count = rice_ball_counts.values().copied().max().unwrap_or(0);

        if max_rice_ball_count > 0 {
            let players_with_max = rice_ball_counts
                .iter()
                .filter(|&(_player_id, &c)| c == max_rice_ball_count)
                .collect::<Vec<_>>();

            if players_with_max.len() > 1 {
                let share = 6 / (Score::try_from(players_with_max.len()).unwrap());
                for (player_id, _) in players_with_max {
                    let score = scores.entry(*player_id).or_insert(0);
                    *score += share;
                }
            } else {
                let (player_id, _rolls) = players_with_max[0];
                let score = scores.entry(*player_id).or_insert(0);
                *score += 6;

                let second_max = rice_ball_counts
                    .values()
                    .copied()
                    .filter(|&p| p != max_rice_ball_count)
                    .max()
                    .unwrap_or(0);

                let players_with_second_max = rice_ball_counts
                    .iter()
                    .filter(|&(_player_id, &c)| c == second_max)
                    .collect::<Vec<_>>();

                let second_share = 3 / (Score::try_from(players_with_second_max.len()).unwrap());
                for (player_id, _) in players_with_second_max {
                    let score = scores.entry(*player_id).or_insert(0);
                    *score += second_share;
                }
            }
        }
    }

    /* sushi */
    {
        for player in players {
            let tea_score: Score = player
                .picked
                .iter()
                .filter(|&cards| cards.len() == 2 && cards[0].card_type() == Card::Tea)
                .map(|cards| cards[1].score_value() * 3)
                .sum();

            let singles_score: Score = player
                .picked
                .iter()
                .filter(|&cards| cards.len() == 1)
                .map(|cards| cards[0].score_value())
                .sum();

            if let Some(score) = scores.get_mut(&player.id) {
                *score += tea_score + singles_score;
            }
        }
    }

    /* tempura */
    {
        for player in players {
            let tempura_count = player
                .picked
                .iter()
                .filter(|&cards| cards.len() == 1 && cards[0].card_type() == Card::Tempura)
                .count();

            let points = Score::try_from((tempura_count / 2) * 5).unwrap();
            if let Some(score) = scores.get_mut(&player.id) {
                *score += points;
            }
        }
    }

    /* dango */
    {
        for player in players {
            let dango_count = player
                .picked
                .iter()
                .filter(|&cards| cards.len() == 1 && cards[0].card_type() == Card::Dango)
                .count();

            let points = Score::try_from((dango_count / 3) * 10).unwrap();
            if let Some(score) = scores.get_mut(&player.id) {
                *score += points;
            }
        }
    }

    /* gyoza */
    {
        for player in players {
            let gyoza_count = player
                .picked
                .iter()
                .filter(|&cards| cards.len() == 1 && cards[0].card_type() == Card::Gyoza)
                .count();
            let points = match gyoza_count {
                0 => 0,
                1 => 1,
                2 => 3,
                3 => 6,
                4 => 10,
                _ => 15,
            };
            if let Some(score) = scores.get_mut(&player.id) {
                *score += points;
            }
        }
    }

    scores
}

impl TryFrom<CardId> for Card {
    type Error = ();

    fn try_from(value: CardId) -> Result<Self, Self::Error> {
        use Card::*;

        match value {
            CardId(1..=14) => Ok(Tempura),
            CardId(15..=28) => Ok(Dango),
            CardId(29..=42) => Ok(Gyoza),
            CardId(43..=54) => Ok(RiceBall(2)),
            CardId(55..=62) => Ok(RiceBall(3)),
            CardId(63..=68) => Ok(RiceBall(1)),
            CardId(69..=78) => Ok(Sushi(2)),
            CardId(79..=83) => Ok(Sushi(3)),
            CardId(84..=88) => Ok(Sushi(1)),
            CardId(89..=98) => Ok(Dessert),
            CardId(99..=104) => Ok(Tea),
            CardId(105..=108) => Ok(TakeoutBox),

            _ => Err(()),
        }
    }
}

#[derive(Clone, Debug, Deserialize, Eq, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct Settings {
    pub player_counts: HashSet<u8>,
}

#[derive(Clone, Debug)]
pub struct GameState {
    pub players: Vec<PlayerState>,
    deck: Vec<CardId>,
    phase: Phase,
    rounds: Vec<HashMap<PlayerId, Score>>,
}

impl GameState {
    pub fn from_dto(public_state: PublicState, secret_state: SecretState) -> GameState {
        let mut private_states = secret_state.private_states();
        GameState {
            players: public_state
                .players
                .into_iter()
                .map(|p| {
                    let private_state = private_states.remove(&p.id).unwrap();
                    PlayerState::from(p, private_state)
                })
                .collect(),
            phase: public_state.phase.to_phase(&secret_state),
            deck: secret_state.deck,
            rounds: public_state.rounds,
        }
    }
    pub fn to_dto(self) -> (PublicState, SecretState) {
        let public_state = PublicState::from(&self);
        let secret_state = SecretState::from(self);

        (public_state, secret_state)
    }

    pub fn find_player(&self, player_id: PlayerId) -> Option<&PlayerState> {
        self.players.iter().find(|p| p.id == player_id)
    }
    pub fn find_player_mut(&mut self, player_id: PlayerId) -> Option<&mut PlayerState> {
        self.players.iter_mut().find(|p| p.id == player_id)
    }

    fn clear_non_dessert_cards(&mut self) {
        for player in self.players.iter_mut() {
            player
                .picked
                .retain(|c| c.len() == 1 && c[0].card_type() == Card::Dessert);
        }
    }
    fn draw_new_player_hands(&mut self) {
        let hand_size = cards_per_player(self.players.len());
        for (player_idx, player) in self.players.iter_mut().enumerate() {
            player.hand = Hand {
                id: player_idx,
                cards: self.deck.split_off(self.deck.len() - hand_size),
            }
        }
    }
}

#[derive(Clone, Debug)]
pub struct PlayerState {
    pub id: PlayerId,
    hand: Hand,
    pick: Option<Pick>,
    picked: Vec<Vec<CardId>>,
    score: Score,
}

impl PlayerState {
    fn from(public_player: PublicPlayerState, private_state: PrivateState) -> Self {
        PlayerState {
            id: public_player.id,
            hand: Hand {
                id: public_player.hand_id,
                cards: private_state.hand,
            },
            pick: if private_state.pick.is_empty() {
                None
            } else {
                Some(Pick {
                    cards: private_state.pick,
                    swap: private_state.swap,
                })
            },
            picked: public_player.picked,
            score: public_player.score,
        }
    }
}

#[derive(Clone, Debug)]
pub struct Hand {
    pub id: usize,
    pub cards: Vec<CardId>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(tag = "k", content = "v")]
pub enum Phase {
    Drafting {},
    Swapping {
        players: Vec<PlayerId>,
        ready: HashSet<PlayerId>,
    },
    Reveal {
        ready: HashSet<PlayerId>,
    },
    RoundEnd {
        ready: HashSet<PlayerId>,
    },
    GameEnd {
        dessert: HashMap<PlayerId, Score>,
    },
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Pick {
    cards: Vec<CardId>,
    swap: bool,
}

#[derive(Clone, Debug, Deserialize, PartialEq, Eq)]
#[serde(tag = "k")]
pub enum Action {
    Pick(Pick),
    Undo,
    Swap { card: CardId },
    Ready,
}

#[cfg(test)]
mod action_test {
    use serde_json::json;

    use super::*;

    #[test]
    fn deserialize_pick_action() {
        let json = json!({ "k": "Pick", "cards": [1], "swap": false });
        let action: Action = serde_json::from_value(json).unwrap();
        assert_eq!(
            action,
            Action::Pick(Pick {
                cards: vec![CardId(1)],
                swap: false
            })
        )
    }
}

/* Move all players' hands to the next player.
 * The player at `idx + 1` sits on the left of the player at `idx`.
 */
fn shift_hands_clockwise(players: &mut Vec<PlayerState>) {
    let mut tmp = Hand {
        id: 0,
        cards: vec![],
    };

    for player in players.iter_mut() {
        std::mem::swap(&mut player.hand, &mut tmp);
    }
    if let Some(player) = players.first_mut() {
        player.hand = tmp;
    }
}

#[cfg(test)]
mod shift_hands_test {
    use super::*;

    #[test]
    fn shift_two_player_hands() {
        let player1 = PlayerState {
            id: 1,
            hand: Hand {
                id: 1,
                cards: vec![CardId(1)],
            },
            picked: vec![],
            pick: None,
            score: 0,
        };
        let player2 = PlayerState {
            id: 2,
            hand: Hand {
                id: 2,
                cards: vec![CardId(2)],
            },
            picked: vec![],
            pick: None,
            score: 0,
        };
        let mut players = vec![player1, player2];
        shift_hands_clockwise(&mut players);

        assert_eq!(players[0].id, 1);
        assert_eq!(players[0].hand.cards, vec![CardId(2)]);

        assert_eq!(players[1].id, 2);
        assert_eq!(players[1].hand.cards, vec![CardId(1)]);
    }
}

fn cards_per_player(player_count: usize) -> usize {
    match player_count {
        2 => 10,
        3 => 9,
        4 => 8,
        5 => 7,
        _ => 0,
    }
}
pub fn initial_state(
    players_ids: &[PlayerId],
    _settings: &Settings,
    seed: i64,
) -> Option<GameState> {
    let mut rng = rand_pcg::Pcg32::from_seed(i128::from(seed).to_le_bytes());

    let hand_size = cards_per_player(players_ids.len());

    let mut deck = (1..=108).map(CardId).collect::<Vec<_>>();
    deck.shuffle(&mut rng);

    let players = players_ids
        .iter()
        .map(|&id| PlayerState {
            id,
            score: 0,
            hand: Hand {
                id: id.unsigned_abs() as usize,
                cards: deck.split_off(deck.len() - hand_size),
            },
            picked: vec![],
            pick: None,
        })
        .collect::<Vec<_>>();

    Some(GameState {
        players,
        deck,
        phase: Phase::Drafting {},
        rounds: vec![],
    })
}

fn add_to_picked_cards(picked: &mut Vec<Vec<CardId>>, card_id: CardId) {
    let card = card_id.card_type();

    if let Some(stack) = picked
        .iter_mut()
        .find(|stack| card.is_nigiri() && stack.len() == 1 && stack[0].card_type() == Card::Tea)
    {
        stack.push(card_id);
    } else {
        picked.push(vec![card_id])
    }
}

pub fn perform_action(
    previous_state: &GameState,
    action: &Action,
    _settings: &Settings,
    performed_by: PlayerId,
) -> Option<GameState> {
    let actor = previous_state.find_player(performed_by)?;

    if !action_is_allowed(action, previous_state, actor) {
        return None;
    }

    let mut state = previous_state.clone();

    match &previous_state.phase {
        Phase::Drafting { .. } => match action {
            Action::Pick(pick) => {
                let picked_card = pick.cards[0];
                let actor_mut = state.find_player_mut(performed_by)?;
                let card_idx = actor_mut
                    .hand
                    .cards
                    .iter()
                    .position(|&c| c == picked_card)?;
                actor_mut.hand.cards.remove(card_idx);

                actor_mut.pick = Some(pick.clone());

                if state.players.iter().all(|p| p.pick.is_some()) {
                    if state.players.iter().any(|p| p.pick.as_ref().unwrap().swap) {
                        let mut swappers: Vec<PlayerId> = vec![];

                        for player in state
                            .players
                            .iter_mut()
                            .filter(|p| p.pick.as_ref().unwrap().swap)
                        {
                            swappers.push(player.id);

                            let takeout_box_idx = player
                                .picked
                                .iter()
                                .position(|s| s.len() == 1 && s[0].is_takeout_box())?;

                            let takeout_box_card = player.picked.remove(takeout_box_idx)[0];
                            player.hand.cards.push(takeout_box_card);
                        }

                        state.phase = Phase::Swapping {
                            players: swappers,
                            ready: HashSet::new(),
                        };
                    } else {
                        state.phase = Phase::Reveal {
                            ready: HashSet::new(),
                        };
                    }
                }
            }
            Action::Undo => {
                let actor_mut = state.find_player_mut(performed_by)?;
                let mut pick = actor_mut.pick.take()?;

                actor_mut.hand.cards.append(&mut pick.cards);
            }
            _ => return None,
        },
        Phase::Swapping { .. } => match action {
            &Action::Swap { card } => {
                let actor_mut = state.find_player_mut(performed_by)?;
                let card_idx = actor_mut.hand.cards.iter().position(|&c| c == card)?;
                actor_mut.hand.cards.remove(card_idx);
                actor_mut
                    .pick
                    .iter_mut()
                    .for_each(|pick| pick.cards.push(card));

                if let Phase::Swapping {
                    ref players,
                    ref mut ready,
                } = state.phase
                {
                    ready.insert(actor.id);

                    if players.iter().all(|p| ready.contains(p)) {
                        state.phase = Phase::Reveal {
                            ready: HashSet::new(),
                        }
                    }
                }
            }
            Action::Undo => {
                let actor_mut = state.find_player_mut(performed_by)?;
                let card = actor_mut.pick.as_mut()?.cards.pop()?;
                actor_mut.hand.cards.push(card);

                if let Phase::Swapping { ref mut ready, .. } = state.phase {
                    ready.remove(&actor.id);
                }
            }
            _ => return None,
        },
        Phase::Reveal { .. } => match action {
            Action::Ready => {
                if let Phase::Reveal { ref mut ready } = state.phase {
                    ready.insert(actor.id);

                    if state.players.iter().all(|p| ready.contains(&p.id)) {
                        for player_mut in state.players.iter_mut() {
                            let pick = player_mut.pick.take()?;

                            for &card_id in pick.cards.iter() {
                                add_to_picked_cards(&mut player_mut.picked, card_id);
                            }
                        }

                        shift_hands_clockwise(&mut state.players);

                        if state.players.iter().any(|p| p.hand.cards.is_empty()) {
                            let scores = score_round(&state.players);
                            for player in state.players.iter_mut() {
                                player.score += scores.get(&player.id).unwrap_or(&0);
                            }
                            state.rounds.push(scores);

                            if state.rounds.len() == 3 {
                                let dessert = score_desserts(&state.players);
                                for (&player_id, &points) in dessert.iter() {
                                    state.find_player_mut(player_id)?.score += points;
                                }
                                state.phase = Phase::GameEnd { dessert }
                            } else {
                                state.phase = Phase::RoundEnd {
                                    ready: HashSet::new(),
                                }
                            }
                        } else {
                            state.phase = Phase::Drafting {};
                        }
                    }
                }
            }
            _ => return None,
        },
        Phase::RoundEnd { .. } => match action {
            Action::Ready => {
                if let Phase::RoundEnd { ready } = &mut state.phase {
                    ready.insert(actor.id);

                    if state.players.iter().all(|p| ready.contains(&p.id)) {
                        state.draw_new_player_hands();
                        state.clear_non_dessert_cards();

                        state.phase = Phase::Drafting {};
                    }
                } else {
                    return None;
                }
            }
            _ => return None,
        },
        Phase::GameEnd { .. } => return None,
    }

    Some(state)
}

fn action_is_allowed(action: &Action, state: &GameState, actor: &PlayerState) -> bool {
    match &state.phase {
        Phase::Drafting {} => match action {
            Action::Pick(Pick { cards, swap }) => {
                actor.pick.is_none()
                    && cards.len() == 1
                    && actor.hand.cards.contains(&cards[0])
                    && (!swap
                        || (actor.hand.cards.len() > 1 /* must have at least 2 cards to use takeout box */
                            && actor
                                .picked
                                .iter()
                                .any(|c| c[0].card_type() == Card::TakeoutBox)))
            }
            Action::Undo => actor.pick.is_some(),
            _ => false,
        },
        Phase::Swapping { players, ready } => match action {
            Action::Swap { card } => {
                players.contains(&actor.id)
                    && !ready.contains(&actor.id)
                    && match &actor.pick {
                        None => false,
                        Some(pick) => pick.cards.len() == 1 && actor.hand.cards.contains(card),
                    }
            }
            Action::Undo => {
                players.contains(&actor.id)
                    && ready.contains(&actor.id)
                    && match &actor.pick {
                        None => false,
                        Some(pick) => pick.cards.len() == 2,
                    }
            }
            _ => false,
        },
        Phase::Reveal { ready, .. } => match action {
            Action::Ready => !ready.contains(&actor.id),
            _ => false,
        },
        Phase::RoundEnd { ready, .. } => match action {
            Action::Ready => !ready.contains(&actor.id),
            _ => false,
        },
        Phase::GameEnd { .. } => false,
    }
}

pub fn is_game_complete(state: &GameState) -> bool {
    match state.phase {
        Phase::GameEnd { .. } => true,
        _ => false,
    }
}
