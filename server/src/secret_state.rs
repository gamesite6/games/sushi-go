use std::collections::HashMap;

use serde::{Deserialize, Serialize};
use serde_tuple::Serialize_tuple;

use crate::{CardId, GameState, Pick, PlayerId, PlayerState};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct SecretState {
    pub deck: Vec<CardId>,
    pub players: HashMap<PlayerId, SecretPlayerState>,
}

impl SecretState {
    pub fn private_states(&self) -> HashMap<PlayerId, PrivateState> {
        self.players
            .iter()
            .map(|(&player_id, secret_player)| {
                let (pick, swap) = if let Some(p) = secret_player.pick.clone() {
                    (p.cards, p.swap)
                } else {
                    (vec![], false)
                };

                (
                    player_id,
                    PrivateState {
                        hand: secret_player.hand.clone(),
                        pick,
                        swap,
                    },
                )
            })
            .collect()
    }
}

impl From<GameState> for SecretState {
    fn from(state: GameState) -> Self {
        SecretState {
            deck: state.deck,
            players: state
                .players
                .into_iter()
                .map(|player| (player.id, SecretPlayerState::from(player)))
                .collect(),
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct SecretPlayerState {
    hand: Vec<CardId>,
    pick: Option<Pick>,
}

impl From<PlayerState> for SecretPlayerState {
    fn from(player: PlayerState) -> Self {
        SecretPlayerState {
            hand: player.hand.cards.into_iter().collect(),
            pick: player.pick,
        }
    }
}

#[derive(Debug, Serialize_tuple)]
pub struct PrivateState {
    pub hand: Vec<CardId>,
    pub pick: Vec<CardId>,
    pub swap: bool,
}
