use std::collections::{HashMap, HashSet};

use crate::{CardId, GameState, Phase, PlayerId, PlayerState, Score, SecretState};
use serde::{Deserialize, Serialize};
use serde_tuple::*;

#[derive(Clone, Debug, Serialize_tuple, Deserialize_tuple)]
pub struct PublicState {
    pub deck: usize,
    pub players: Vec<PublicPlayerState>,
    pub phase: PublicPhase,
    pub rounds: Vec<HashMap<PlayerId, Score>>,
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub enum PublicPhase {
    #[serde(rename = "a")]
    Drafting(Drafting),

    #[serde(rename = "b")]
    Swapping(Swapping),

    #[serde(rename = "c")]
    Reveal(Reveal),

    #[serde(rename = "d")]
    RoundEnd(RoundEnd),

    #[serde(rename = "e")]
    GameEnd(GameEnd),
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub struct Drafting();

#[derive(Clone, Debug, Serialize_tuple, Deserialize_tuple, PartialEq)]
pub struct Swapping {
    players: Vec<PlayerId>,
    ready: HashSet<PlayerId>,
}

#[derive(Clone, Debug, Serialize_tuple, Deserialize_tuple, PartialEq)]
pub struct Reveal {
    picks: HashMap<PlayerId, Vec<CardId>>,
    ready: HashSet<PlayerId>,
}

#[derive(Clone, Debug, Serialize_tuple, Deserialize_tuple, PartialEq)]
pub struct RoundEnd {
    ready: HashSet<PlayerId>,
}

#[derive(Clone, Debug, Serialize_tuple, Deserialize_tuple, PartialEq)]
pub struct GameEnd {
    dessert: HashMap<PlayerId, Score>,
}

#[cfg(test)]
mod public_phase_test {
    use std::collections::{HashMap, HashSet};
    use std::iter::FromIterator;

    use serde_json::json;

    use crate::public_state::{Drafting, Reveal};

    use super::PublicPhase;

    #[test]
    fn serialize_deserialize_drafting() {
        let drafting = PublicPhase::Drafting(Drafting());
        let expected_drafting = json!({ "a": [] });
        assert_eq!(serde_json::to_value(&drafting).unwrap(), expected_drafting,);

        let deserialized_drafting: PublicPhase = serde_json::from_value(expected_drafting).unwrap();
        assert_eq!(deserialized_drafting, drafting,);
    }

    #[test]
    fn serialzie_deserialize_reveal() {
        let reveal = PublicPhase::Reveal(Reveal {
            picks: HashMap::from_iter([(1, vec![])]),
            ready: HashSet::from_iter([1]),
        });
        let expected_reveal = json!({ "c": [{"1": [] }, [1] ]});
        assert_eq!(serde_json::to_value(&reveal).unwrap(), expected_reveal);

        let deserialized_reveal: PublicPhase = serde_json::from_value(expected_reveal).unwrap();
        assert_eq!(deserialized_reveal, reveal);
    }
}

impl PublicPhase {
    pub fn to_phase(self, _secret_state: &SecretState) -> Phase {
        match self {
            PublicPhase::Drafting(Drafting()) => Phase::Drafting {},
            PublicPhase::Swapping(Swapping { players, ready }) => {
                Phase::Swapping { players, ready }
            }
            PublicPhase::Reveal(Reveal { ready, .. }) => Phase::Reveal { ready },
            PublicPhase::RoundEnd(RoundEnd { ready }) => Phase::RoundEnd { ready },
            PublicPhase::GameEnd(GameEnd { dessert }) => Phase::GameEnd { dessert },
        }
    }
}

impl From<&GameState> for PublicPhase {
    fn from(game: &GameState) -> Self {
        match &game.phase {
            Phase::Drafting { .. } => Self::Drafting(Drafting()),

            Phase::Swapping { players, ready } => Self::Swapping(Swapping {
                players: players.clone(),
                ready: ready.clone(),
            }),
            Phase::Reveal { ready, .. } => {
                let picks = game
                    .players
                    .iter()
                    .map(|p| {
                        (
                            p.id,
                            p.pick.clone().map(|pick| pick.cards).unwrap_or(vec![]),
                        )
                    })
                    .collect();
                Self::Reveal(Reveal {
                    ready: ready.clone(),
                    picks,
                })
            }
            Phase::RoundEnd { ready } => Self::RoundEnd(RoundEnd {
                ready: ready.clone(),
            }),
            Phase::GameEnd { dessert } => Self::GameEnd(GameEnd {
                dessert: dessert.clone(),
            }),
        }
    }
}

impl From<&GameState> for PublicState {
    fn from(state: &GameState) -> Self {
        PublicState {
            deck: state.deck.len(),
            players: state
                .players
                .clone()
                .into_iter()
                .map(PublicPlayerState::from)
                .collect(),
            phase: PublicPhase::from(state),
            rounds: state.rounds.clone(),
        }
    }
}

#[derive(Clone, Debug, Serialize_tuple, Deserialize_tuple)]
pub struct PublicPlayerState {
    pub id: PlayerId,
    pub hand_cards: usize,
    pub hand_id: usize,
    pub pick: usize,
    pub picked: Vec<Vec<CardId>>,
    pub score: i8,
}

impl From<PlayerState> for PublicPlayerState {
    fn from(player: PlayerState) -> Self {
        PublicPlayerState {
            id: player.id,
            hand_cards: player.hand.cards.len(),
            hand_id: player.hand.id,
            pick: player.pick.map(|p| p.cards.len()).unwrap_or(0),
            picked: player.picked,
            score: player.score,
        }
    }
}
