use actix_web::{post, web, App, HttpResponse, HttpServer, Responder};

use serde::{Deserialize, Serialize};

use std::{collections::HashMap, env};
use tokyo_takeout_server::*;

#[post("/info")]
async fn info_endpoint(req: web::Json<InfoReq>) -> impl Responder {
    let selected_counts = req.into_inner().settings.player_counts;

    let player_counts = [2, 3, 4, 5]
        .iter()
        .filter(|c| selected_counts.contains(c))
        .copied()
        .collect::<Vec<_>>();

    HttpResponse::Ok().json(InfoRes { player_counts })
}

#[post("/initial-state")]
async fn initial_state_endpoint(req: web::Json<InitialStateReq>) -> impl Responder {
    match initial_state(&req.players, &req.settings, req.seed) {
        Some(state) => {
            let (public_state, secret_state) = state.to_dto();
            let private_states = secret_state.private_states();

            HttpResponse::Ok().json(InitialStateRes {
                state: public_state,
                secret_state,
                private_states,
            })
        }
        None => HttpResponse::UnprocessableEntity().finish(),
    }
}

#[post("/perform-action")]
async fn perform_action_endpoint(req: web::Json<PerformActionReq>) -> impl Responder {
    let req = req.into_inner();
    let state = GameState::from_dto(req.state, req.secret);
    match perform_action(&state, &req.action, &req.settings, req.performed_by) {
        Some(next_state) => {
            let completed = is_game_complete(&next_state);
            let (public_state, secret_state) = next_state.to_dto();
            let private_states = secret_state.private_states();
            HttpResponse::Ok().json(PerformActionRes {
                completed,
                next_state: public_state,
                next_secret: secret_state,
                next_private: private_states,
            })
        }
        None => HttpResponse::UnprocessableEntity().finish(),
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();

    let port: u16 = env::var("PORT").unwrap().parse().unwrap();
    let addr = format!("0.0.0.0:{}", port);

    println!("Listening on {}", addr);

    HttpServer::new(|| {
        App::new()
            .service(info_endpoint)
            .service(initial_state_endpoint)
            .service(perform_action_endpoint)
    })
    .bind(addr)?
    .run()
    .await
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct InfoReq {
    pub settings: Settings,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct InfoRes {
    pub player_counts: Vec<u8>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct InitialStateReq {
    pub players: Vec<PlayerId>,
    pub settings: Settings,
    pub seed: i64,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct InitialStateRes {
    pub state: PublicState,
    pub secret_state: SecretState,
    pub private_states: HashMap<PlayerId, PrivateState>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PerformActionReq {
    pub performed_by: PlayerId,
    pub action: Action,
    pub state: PublicState,
    pub secret: SecretState,
    pub settings: Settings,
    pub seed: i64,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct PerformActionRes {
    pub completed: bool,
    pub next_state: PublicState,
    pub next_secret: SecretState,
    pub next_private: HashMap<PlayerId, PrivateState>,
}
