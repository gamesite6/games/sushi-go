import GameComponent from "./Game.svelte";
import ReferenceComponent from "./Reference.svelte";
import SettingsComponent from "./Settings.svelte";

type GameProps = {
  playerId: PlayerId | null;
  publicState: Server.PublicState;
  privateState: Server.PrivateState;
  settings: Server.GameSettings;
};

export class Game extends HTMLElement {
  #playerId?: PlayerId;
  #publicState?: Server.PublicState;
  #privateState?: Server.PrivateState;
  #settings?: Server.GameSettings;
  #app?: GameComponent;

  set public_state(publicState: Server.PublicState) {
    if (this.#publicState !== publicState) {
      this.#publicState = publicState;
      this.rerender({ publicState });
    }
  }
  set state(state: Server.PublicState) {
    this.public_state = state;
  }

  set private_state(privateState: Server.PrivateState) {
    if (this.#privateState !== privateState) {
      this.#privateState = privateState;
      this.rerender({ privateState });
    }
  }

  set settings(settings: Server.GameSettings) {
    if (this.#settings !== settings) {
      this.#settings = settings;
      this.rerender({ settings });
    }
  }

  set player_id(playerId: PlayerId | undefined) {
    this.playerid = playerId;
  }
  set playerid(playerId: PlayerId | undefined) {
    if (this.#playerId !== playerId) {
      this.#playerId = playerId;
      this.rerender({ playerId: playerId ?? null });
    }
  }

  async connectedCallback() {
    if (this.#settings && this.#publicState) {
      this.#app = new GameComponent({
        target: this,
        props: {
          playerId: this.#playerId ?? null,
          privateState: this.#privateState ?? null,
          publicState: this.#publicState,
          // settings: this.#settings,
        },
      });

      this.#app.$on("action", (evt: CustomEvent) => {
        this.dispatchEvent(new CustomEvent("action", { detail: evt.detail }));
      });
    }
  }

  private rerender(changedProps: Partial<GameProps>) {
    if (this.#app) {
      this.#app.$set(changedProps);
    }
  }
}

type ReferenceProps = {
  state: Server.PublicState;
  settings: Server.GameSettings;
};

export class Reference extends HTMLElement {
  #settings?: Server.GameSettings;
  #state?: Server.PublicState;
  #app?: ReferenceComponent;

  set state(state: Server.PublicState) {
    if (this.#state !== state) {
      this.#state = state;
      this.rerender({ state });
    }
  }

  set settings(settings: Server.GameSettings) {
    if (this.#settings !== settings) {
      this.#settings = settings;
      this.rerender({ settings });
    }
  }

  connectedCallback() {
    if (this.#settings && this.#state) {
      this.#app = new ReferenceComponent({
        target: this,
        props: {
          // settings: this.#settings,
          state: this.#state,
        },
      });
    }
  }

  private rerender(changedProps: Partial<ReferenceProps>) {
    if (this.#app) {
      this.#app.$set(changedProps);
    }
  }
}

import { getDefaultSettings } from "./settings";
const defaultSettings = getDefaultSettings();
export { defaultSettings };
export { playerCounts } from "./settings";

type SettingsProps = {
  settings?: Server.GameSettings;
  readonly?: boolean;
};

export class Settings extends HTMLElement {
  #app?: SettingsComponent;
  #settings?: Server.GameSettings;

  set settings(settings: Server.GameSettings) {
    if (this.#settings !== settings) {
      this.#settings = settings;
      this.rerender({ settings });
    }
  }

  get #readonly(): boolean {
    const attr = this.attributes.getNamedItem("readonly");
    return attr !== null && attr.value !== "false";
  }

  connectedCallback() {
    this.#app = new SettingsComponent({
      target: this,
      props: {
        readonly: this.#readonly,
        settings: this.#settings,
      },
    });

    this.#app.$on("settings_change", (evt: CustomEvent) => {
      this.dispatchEvent(
        new CustomEvent("settings_change", {
          detail: evt.detail,
        })
      );
    });
  }

  private rerender(changedProps: Partial<SettingsProps>) {
    if (this.#app) {
      this.#app.$set(changedProps);
    }
  }

  static get observedAttributes() {
    return ["readonly"];
  }

  attributeChangedCallback(name: string, oldValue: any, newValue: any) {
    if (name === "readonly") {
      this.rerender({ readonly: this.#readonly });
    }
  }
}
