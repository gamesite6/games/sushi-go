/// <reference types="svelte" />
/// <reference types="vite/client" />

type PlayerId = number;
type CardId = number;

namespace Server {
  type HandSize = number;
  type HandId = number;
  type PickSize = number;
  type PreviousPicks = CardId[][];
  type Score = number;

  type PublicPlayerState = [
    PlayerId,
    HandSize,
    HandId,
    PickSize,
    PreviousPicks,
    Score
  ];

  type DeckSize = number;

  type PublicState = [DeckSize, PublicPlayerState[], Phase, PlayerPoints[]];

  type PrivateState = [CardId[], CardId[], boolean];

  type Phase = {
    a?: [];
    b?: [PlayerId[], PlayerId[]];
    c?: [{ [playerId: string]: CardId[] }, PlayerId[]];
    d?: [PlayerId[]];
    e?: [PlayerPoints];
  };

  type PlayerPoints = { [playerId: string]: number };

  type RoundEndPhase = {
    ready: PlayerId[];
  };

  type GameSettings = {
    playerCounts: number[];
  };

  type Action =
    | { k: "Pick"; cards: CardId[]; swap: boolean }
    | { k: "Undo" }
    | { k: "Swap"; card: CardId }
    | { k: "Ready" };
}
