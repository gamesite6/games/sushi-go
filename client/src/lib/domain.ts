import { CardType, getCardType } from "./utils";

export class GameState {
  #phase: Phase;
  #deckSize: number;
  #players: PlayerState[];
  #ready: PlayerId[];
  #swapping: PlayerId[];
  #roundsPlayed: number;

  static fromPublicState(s: Server.PublicState) {
    let [deck, players, phase, rounds] = s;

    let ready: PlayerId[] = [];
    let swapping: PlayerId[] = [];

    let playerPicks: { [playerId: string]: CardId[] } = {};
    let dessertScores: { [playerId: string]: number } = {};

    if (phase.a) {
    } else if (phase.b) {
      [swapping, ready] = phase.b;
    } else if (phase.c) {
      [playerPicks, ready] = phase.c;
    } else if (phase.d) {
      [ready] = phase.d;
    } else if (phase.e) {
      [dessertScores] = phase.e;
    }

    return new GameState({
      ready,
      swapping,
      phase: phaseFromServer(phase),
      players: players.map((p) => {
        let [playerId] = p;
        return PlayerState.fromServer(p, {
          currentPick: playerPicks[playerId],
          dessertScore: dessertScores[playerId],
          rounds: rounds.map((r) => r[playerId]),
        });
      }),
      deckSize: deck,
      roundsPlayed: rounds.length,
    });
  }

  constructor(data: {
    phase: Phase;
    players: PlayerState[];
    swapping: PlayerId[];
    ready: PlayerId[];
    deckSize: number;
    roundsPlayed: number;
  }) {
    this.#phase = data.phase;
    this.#players = data.players;
    this.#ready = data.ready;
    this.#swapping = data.swapping;
    this.#deckSize = data.deckSize;
    this.#roundsPlayed = data.roundsPlayed;
  }
  phase(): Phase {
    return this.#phase;
  }
  deckSize(): number {
    return this.#deckSize;
  }

  swappingPlayers(): PlayerId[] {
    return this.#swapping;
  }

  isPlayerActive(playerId: PlayerId): boolean {
    switch (this.#phase) {
      case Phase.Drafting:
        return this.#players.some(
          (p) => p.id() === playerId && p.pickSize() === 0
        );

      case Phase.Swapping:
        return (
          this.#swapping.includes(playerId) && !this.#ready.includes(playerId)
        );

      case Phase.Reveal:
        return !this.#ready.includes(playerId);

      case Phase.RoundEnd:
        return !this.#ready.includes(playerId);

      default:
        return false;
    }
  }

  players(): PlayerState[] {
    return this.#players;
  }

  winners(): PlayerId[] {
    let maxScore = Math.max(...this.players().map((p) => p.score()));
    return this.players()
      .filter((p) => p.score() === maxScore)
      .map((p) => p.id());
  }

  playerOrder(firstPlayer: PlayerId | null): PlayerId[] {
    const playerIds = this.#players.map((p) => p.id());
    const idx = firstPlayer !== null ? playerIds.indexOf(firstPlayer) : -1;
    if (idx !== -1) {
      return [...playerIds.slice(idx), ...playerIds.slice(0, idx)];
    } else {
      return playerIds;
    }
  }

  roundsPlayed(): number {
    return this.#roundsPlayed;
  }
}

export enum Phase {
  Drafting,
  Swapping,
  Reveal,
  RoundEnd,
  GameEnd,
}

function phaseFromServer(phase: Server.Phase): Phase {
  if (phase.a) {
    return Phase.Drafting;
  } else if (phase.b) {
    return Phase.Swapping;
  } else if (phase.c) {
    return Phase.Reveal;
  } else if (phase.d) {
    return Phase.RoundEnd;
  } else {
    return Phase.GameEnd;
  }
}

export class PlayerState {
  static fromServer(
    p: Server.PublicPlayerState,
    data: {
      currentPick: CardId[] | undefined;
      dessertScore: number | undefined;
      rounds: number[] | undefined;
    }
  ): PlayerState {
    const [id, handSize, handId, pickSize, previousPicks, score] = p;
    return new PlayerState({
      id,
      handId,
      handSize,
      pickSize,
      score,
      currentPick: data.currentPick ?? null,
      previousPicks,
      roundScores: data.rounds ?? null,
      dessertScore: data.dessertScore ?? null,
    });
  }

  #id: PlayerId;
  #handId: number;
  #handSize: number;
  #pick: CardId[] | null;
  #pickSize: number;
  #score: number;
  #previousPicks: CardId[][];
  #roundScores: number[] | null;
  #dessertScore: number | null;

  constructor(data: {
    id: PlayerId;
    handId: number;
    handSize: number;
    pickSize: number;
    score: number;
    currentPick: CardId[] | null;
    previousPicks: CardId[][];
    roundScores: number[] | null;
    dessertScore: number | null;
  }) {
    this.#id = data.id;
    this.#handId = data.handId;
    this.#handSize = data.handSize;
    this.#pickSize = data.pickSize;
    this.#score = data.score;
    this.#pick = data.currentPick;
    this.#previousPicks = data.previousPicks;
    this.#roundScores = data.roundScores;
    this.#dessertScore = data.dessertScore;
  }

  id(): PlayerId {
    return this.#id;
  }
  handId(): number {
    return this.#handId;
  }
  handSize(): number {
    return this.#handSize;
  }
  pickSize(): number {
    return this.#pickSize;
  }
  score(): number {
    return this.#score;
  }
  currentPick(): CardId[] | null {
    return this.#pick;
  }
  previousPicks(): CardId[][] {
    return this.#previousPicks;
  }
  hasSwapAvailable(): boolean {
    return (
      this.handSize() > 1 &&
      this.previousPicks()
        .flat()
        .some((c) => getCardType(c) === CardType.TakeoutBox)
    );
  }
  roundScore(roundIdx: number): number | null {
    return this.#roundScores?.[roundIdx] ?? null;
  }
  dessertScore(): number | null {
    return this.#dessertScore;
  }
}

export class PrivateState {
  static fromServer(s: Server.PrivateState): PrivateState {
    const [hand, pickedCards, usedSwap] = s;
    return new PrivateState({
      hand,
      usedSwap,
      pickedCards,
    });
  }

  #hand: CardId[];
  #usedSwap: boolean;
  #pickedCards: CardId[];

  constructor(data: {
    hand: CardId[];
    usedSwap: boolean;
    pickedCards: CardId[];
  }) {
    this.#hand = [...data.hand];
    this.#hand.sort((a, b) => a - b);

    this.#usedSwap = data.usedSwap;
    this.#pickedCards = data.pickedCards;
  }

  hand(): CardId[] {
    return this.#hand;
  }
  usedSwap(): boolean {
    return this.#usedSwap;
  }
  pickedCards(): CardId[] {
    return this.#pickedCards;
  }
}
