import { describe, test, expect } from "vitest";
import { GameState, Phase, PlayerState } from "./domain";

describe("game state", () => {
  describe("drafting phase", () => {
    test("players who have not picked a card are active", () => {
      let game = new GameState({
        phase: Phase.Drafting,
        players: [
          new PlayerState({
            id: 1,
            handSize: 7,
            pickSize: 0,
            currentPick: [],
            score: 0,
            handId: 1,
            previousPicks: [],
            dessertScore: null,
            roundScores: null,
          }),
          new PlayerState({
            id: 2,
            handSize: 7,
            pickSize: 1,
            currentPick: [],
            score: 0,
            handId: 2,
            previousPicks: [],
            dessertScore: null,
            roundScores: null,
          }),
        ],
        ready: [],
        swapping: [],
        deckSize: 50,
        roundsPlayed: 0,
      });

      expect(game.isPlayerActive(1)).toBe(true);
      expect(game.isPlayerActive(2)).toBe(false);
    });
  });
});
