export function range(from: number, to: number): number[] {
  const size = Math.max(to - from, 0);
  const result = new Array(size);

  for (let i = 0; i < size; i++) {
    result[i] = from + i;
  }

  return result;
}

export function rangeInclusive(from: number, to: number): number[] {
  return range(from, to + 1);
}

export function reversed<T>(ts: T[]): T[] {
  let result = [...ts];
  result.reverse();
  return result;
}

export function sorted<T extends number>(ts: T[]): T[] {
  let result = [...ts];
  result.sort((a, b) => a - b);
  return result;
}

export function sortedBy<T>(ts: T[], compare: (a: T, b: T) => number) {
  let result = [...ts];
  result.sort(compare);
  return result;
}

import { createEventDispatcher } from "svelte";

export function createActionDispatcher() {
  const dispatch = createEventDispatcher();
  return function performAction(action: Server.Action) {
    dispatch("action", action);
  };
}

export enum CardType {
  Tempura,
  Dango,
  Gyoza,
  RiceBall,
  Sushi,
  Dessert,
  Tea,
  TakeoutBox,
}

export function getCardType(card: CardId): CardType {
  if (1 <= card && card <= 14) {
    return CardType.Tempura;
  } else if (15 <= card && card <= 28) {
    return CardType.Dango;
  } else if (29 <= card && card <= 42) {
    return CardType.Gyoza;
  } else if (43 <= card && card <= 54) {
    return CardType.RiceBall;
  } else if (55 <= card && card <= 62) {
    return CardType.RiceBall;
  } else if (63 <= card && card <= 68) {
    return CardType.RiceBall;
  } else if (69 <= card && card <= 78) {
    return CardType.Sushi;
  } else if (79 <= card && card <= 83) {
    return CardType.Sushi;
  } else if (84 <= card && card <= 88) {
    return CardType.Sushi;
  } else if (89 <= card && card <= 98) {
    return CardType.Dessert;
  } else if (99 <= card && card <= 104) {
    return CardType.Tea;
  } else if (105 <= card && card <= 108) {
    return CardType.TakeoutBox;
  } else {
    throw new Error("invalid card id: " + card);
  }
}

export function getRiceBallCount(card: CardId): number {
  if (43 <= card && card <= 54) {
    return 2;
  } else if (55 <= card && card <= 62) {
    return 3;
  } else if (63 <= card && card <= 68) {
    return 1;
  } else {
    return 0;
  }
}

export function getSingleScoreValue(card: CardId): number {
  if (69 <= card && card <= 78) {
    return 2;
  } else if (79 <= card && card <= 83) {
    return 3;
  } else if (84 <= card && card <= 88) {
    return 1;
  } else {
    return 0;
  }
}
