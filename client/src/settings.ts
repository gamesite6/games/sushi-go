export function getDefaultSettings() {
  return {
    playerCounts: [3, 4, 5],
  };
}

export const validPlayerCounts = [2, 3, 4, 5];

export function playerCounts(settings: Server.GameSettings): number[] {
  return validPlayerCounts.filter((c) => settings.playerCounts.includes(c));
}
