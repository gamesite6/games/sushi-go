import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";

// https://vitejs.dev/config/
export default defineConfig({
  base: "/static/tokyo-takeout/",
  build: {
    emptyOutDir: false,
    lib: {
      entry: "src/index.ts",
      fileName: "tokyo-takeout",
      name: "Gamesite6_TokyoTakeout",
    },
  },
  css: {
    modules: {},
  },
  plugins: [svelte()],
});
